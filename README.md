init
```shell
pnpm install
```

create profile
```shell
ts-node src/script/profile/create --handle=dwafwafwaff --private-key=0x7ecce33914efdaf0015c9e9ddf3805735d3d88debcacb021be493c6d5ae45c02
```

create profile async
```shell
ts-node src/script/profile/create-async --handle=handle_name --private-key=0x7ecce33914efdaf0015c9e9ddf3805735d3d88debcacb021be493c6d5ae45c02
```

create profile async
预售期创建社区需要绑定的钱包持有somonNFT。dev环境的NFT为：https://sepolia.basescan.org/address/0x567206C8f1519094b60f117f9f2E51b8c04944ff#writeContract 可以自己去mint。
在脚本中，默认会绑定传入参数中私钥对应的地址。需要将NFT mint到该地址。 一个tokenId只能使用一次。
<br/>
根据handle_name的长度需要从aa钱包支付ETH，需要确保aa钱包资金充足具体规则为：
- handle_name长度为1，支付2.049ETH
- handle_name长度为2，支付0.257ETH
- handle_name长度为3，支付0.065ETH
- handle_name长度为4，支付0.017ETH
- handle_name长度为5，支付0.005ETH
- handle_name长度为6，支付0.003ETH
- handle_name长度大于7，支付0.001ETH

```shell
ts-node src/script/community/create-presale-async --handle=handle_name --tags=tag1,tag2 --private-key=0x7ecce33914efdaf0015c9e9ddf3805735d3d88debcacb021be493c6d5ae45c02
```

join all community
```shell
ts-node src/script/community/join-all --private-key=0x7ecce33914efdaf0015c9e9ddf3805735d3d88debcacb021be493c6d5ae45c02
```

join community
```shell
ts-node src/script/community/join --community 0x1,0x2,0x3,0x4,0x5,0x6,0x7 --private-key=0x7ecce33914efdaf0015c9e9ddf3805735d3d88debcacb021be493c6d5ae45c02
```

post
```shell
ts-node src/script/content/create-post --title=post_title --content=post_content --community=0x1 --private-key=0x7ecce33914efdaf0015c9e9ddf3805735d3d88debcacb021be493c6d5ae45c02
```

get userOperationReceipt
```shell
ts-node src/script/user_operation/get-user-operation-receipt --userOperationHash=0x0c35d5bd74c7984e2373a21f25bd12237da4697687ee2703031200ab1a75d554 --private-key=0x7ecce33914efdaf0015c9e9ddf3805735d3d88debcacb021be493c6d5ae45c02
```
