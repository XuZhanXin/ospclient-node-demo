import {catchError, getOspClient} from "../../osp";
import {ethers} from "ethers";
import minimist from "minimist";

async function main() {
    const args = minimist(process.argv.slice(2), {string: ['community']});
    const communityIds = (args['community'] as string).split(',').map(ethers.utils.hexValue);
    const ospClient = await getOspClient();
    const joinRes = await catchError(ospClient.relation.join({community_id: communityIds}));
    console.log('join community success:', joinRes);
}

main().catch(e => {
    console.log('script error:', e);
}).then(() => {
    console.log('script finished');
})
