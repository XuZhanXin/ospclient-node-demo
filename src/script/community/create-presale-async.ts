import {getOspClient} from "../../osp";
import minimist from "minimist";
import {bindWallet, getTribePermit} from "../../somon";
import {ethers} from "ethers";
import {OspClient} from "@open-social-protocol/osp-client";
import {PFP_NFT_ADDRESS, PRE_SALE_CONDITION_ADDRESS} from "../../config";

async function queryAvailablePFP(ospClient: OspClient) {
    //绑定的外部EOA地址
    const provider = ospClient.txClient.ethersProvider;
    const holder = await ospClient.txClient.signer.getAddress();
    console.log(`holder address: ${holder}`);
    // PFP合约地址
    const pfpAddr = PFP_NFT_ADDRESS;
    const res = await provider.call({
        to: pfpAddr,
        data: ethers.utils.solidityPack(['bytes4', 'bytes32'], ['0x8462151c', ethers.utils.defaultAbiCoder.encode(['address'], [holder])])
    });

    const pfpResult = ethers.utils.defaultAbiCoder.decode(['uint256[]'], res)[0];
    console.log(`query PFP result: ${JSON.stringify(pfpResult)}`);
    if (pfpResult.length === 0) {
        console.log('user has no PFP');
        return;
    }
    console.log(`user has PFP: ${JSON.stringify(pfpResult)}`);
    //查询预售 可用PFP
    const presaleSigCondAddress = PRE_SALE_CONDITION_ADDRESS // 预售合约地址(合约提供)
    let newVar = await ospClient.txClient.multicall.callStatic.aggregate(
        pfpResult.map(
            pfpId => ({
                target: presaleSigCondAddress,
                callData: ethers.utils.solidityPack(
                    ['bytes4', 'bytes'],
                    ['0xa6f0e840',
                        ethers.utils.defaultAbiCoder.encode(['address', 'uint256', 'address'], [pfpAddr, pfpId, holder])]),
            })
        )
    );

    const usableResult = newVar.returnData.map((data) => ethers.utils.defaultAbiCoder.decode(['bool'], data)[0] as boolean);
    console.log(`query usable PFP result: ${JSON.stringify(usableResult)}`);
    //找到第一个可用的PFP
    let index = usableResult.findIndex(element => element === true);
    if (index === -1) {
        console.log('no usable PFP');
        return null;
    }
    return pfpResult[index];
    //然后调用后端接口签名 and 获取返回的数据 省略调用后端接口过程 联系下@伟明
    //  let presaleSigCondData = "0x";
    // await ospClient.community.create({
    //     handle: 'test',
    //     community_condition: {
    //         external_cond: {
    //             address: presaleSigCondAddress,
    //             data: presaleSigCondData,
    //             value: await ospClient.community.getHandlePrice("test")
    //         }
    //     },
    //     join_condition: {},
    // })
}

async function main() {
    const ospClient = await getOspClient();

    const args = minimist(process.argv.slice(2), {string: ['handle', 'tags', 'display_name', 'logo', 'banner']});
    const handle = args['handle'] as string;
    const tags = args['tags'] ? [] : (args['tags'] as string).split(',');
    const displayName = args['display_name'] as string;
    const logo = args['logo'] as string;
    const banner = args['banner'] as string;

    const availablePFP = await queryAvailablePFP(ospClient);
    if (!availablePFP) throw new Error('not has pfpNFT');
    const createRreCheckResult = await ospClient.community.checkPreCreate(handle);
    if (createRreCheckResult.handle_taken) throw new Error('handle already taken');
    if (!createRreCheckResult.sufficient) throw new Error(`aa wallet (${await ospClient.txClient.getAccountAddress()}) balance not enough, need ${ethers.utils.formatEther(createRreCheckResult.handle_price)}`);
    // 预售期创建社群需要somon绑定的eoa地址持有pfpNFT。
    await bindWallet(ospClient);
    const tribePermit = await getTribePermit(availablePFP);
    try {
        const operations = await ospClient.community._buildCreateCommunityOperations({
            handle: handle,
            community_condition: {
                external_cond: {
                    address: PRE_SALE_CONDITION_ADDRESS,
                    data: `0x${tribePermit.data}`,
                    value: createRreCheckResult.handle_price
                },
            },
            join_condition: {
                free_join_cond: {}
            },
            tags: tags,
        });
        const hash =await ospClient.txClient.send(operations);
        console.log(`create community op hash: ${hash}`);
    } catch (e) {
        if (JSON.stringify(e).includes('0xe14ddfc8')) {
            throw new Error('not has profile');
        }
    }
}

main().catch(e => {
    console.log('script error:', e);
}).then(() => {
    console.log('script finished');
})
