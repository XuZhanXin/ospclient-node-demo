import {catchError, getOspClient} from "../../osp";
import {CommunityRankingEnum} from "@open-social-protocol/osp-client";
import {BigNumberish} from "ethers";

async function main() {
    const ospClient = await getOspClient();
    const communityPagination = await ospClient.community.getCommunities({
        ranking: CommunityRankingEnum.CREATED,
        limit: 20,
    });
    const communityIds: BigNumberish[] = [];
    for (let community of communityPagination.rows || []) {
        communityIds.push(community.id as string);
    }
    const joinRes = await catchError(ospClient.relation.join({community_id: communityIds}));
    console.log('join community success:', joinRes);
}

main().catch(e => {
    console.log('script error:', e);
}).then(() => {
    console.log('script finished');
})
