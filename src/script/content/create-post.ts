import {getOspClient} from "../../osp";
import {wrapperMetadata} from "@open-social-protocol/osp-client";
import minimist from "minimist";

async function main() {
    const args = minimist(process.argv.slice(2), {string: ['title', 'content', 'community']});
    const title = String(args['title']);
    const content = String(args['content']);
    const community = args['community'];
    const ospClient = await getOspClient();

    // @ts-ignore
    const metadata = wrapperMetadata({content: content, title: title});
    const uri = await ospClient.IPFS.upload(metadata);
    const res = await ospClient.activity.add({
        community_id: community,
        content_uri: uri,
        referenced_condition: {
            only_member_referenced_cond: {}
        },
        is_on_chain: false
    });
    console.log(await ospClient.txClient.getAccountAddress());
    console.log('create post success:', res);
}

main().catch(e => {
    console.log('script error:', e);
}).then(() => {
    console.log('script finished');
})
