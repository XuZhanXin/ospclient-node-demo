import {catchError, getOspClient} from "../../osp";
import {ProfileError, ProfileErrorCodeEnum} from "@open-social-protocol/osp-client";
import minimist from "minimist";

async function main() {
    const ospClient = await getOspClient();
    const args = minimist(process.argv.slice(2),{string:['userOperationHash']});
    const userOperationHash = String(args['userOperationHash']);
    console.log(userOperationHash)
    const receipt = await ospClient.txClient.getReceipt(userOperationHash);
    if(receipt.success){
        console.log('receipt success:', receipt);
    } else {
        console.log('receipt failed:', receipt);
    }
}

main().catch(e => {
    console.log('script error:', e);
}).then(() => {
    console.log('script finished');
})
