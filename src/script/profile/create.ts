import {catchError, getOspClient} from "../../osp";
import {ProfileError, ProfileErrorCodeEnum} from "@open-social-protocol/osp-client";
import minimist from "minimist";

async function main() {
    const ospClient = await getOspClient();
    // const profile = await ospClient.profile.get();
    // if(profile){
    //     throw new Error('profile already exists');
    // }
    const args = minimist(process.argv.slice(2),{string:['handle']});
    const handle = String(args['handle']);
    console.log(handle)
    const profileHandle = new RegExp('^[0-9a-z_]{4,15}$').exec(handle)?.at(0);
    // handle 由 4 - 15 位数字、字母、下划线组成。
    // handle consists of 4 to 15 digits, letters, and underscores.
    if (profileHandle == null) {
        throw new Error("handle name is invalid");
    }
    if ((await ospClient.profile.getByHandle(profileHandle)) != null) {
        // 在后端查询handle是否占用，因为内部环境有许多脏数据，这一步并不能完全检测出handle重复。在下面上链时还会出现一个handle重复的错误码。
        // In the backend, checking if a handle is already in use is not foolproof due to the presence of a lot of dirty data in the internal environment. This step cannot completely detect handle duplicates.  An error code for handle duplication will still occur when attempting to upload to the chain below.
        throw new Error("handle name already exists");
    }
    try {
        // profile是一种SBT，会被mint到用户的AA钱包上。
        // "Profile" is a type of SBT that will be minted to the user's AA wallet.
        const profile =await catchError(ospClient.profile.create({
            handle: handle
        }));
        console.log('create profile:', profile);
    } catch (e) {
        if (e instanceof ProfileError && e.code == ProfileErrorCodeEnum.HANDLE_TAKEN) {
            throw new Error("handle name already exists");
        } else {
            throw e;
        }
    }
}

main().catch(e => {
    console.log('script error:', e);
}).then(() => {
    console.log('script finished');
})
