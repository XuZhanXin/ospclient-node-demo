import {Environment} from "@open-social-protocol/osp-client";
import dotenv from 'dotenv';
import minimist from "minimist";
dotenv.config();

export const ENV:Environment = process.env.ENV as Environment;
console.log(`environment is ${ENV}`);

export const SOMON_URL = {
    prod: "https://api.social.monster/v2",
    pre: "https://api.pre.somon.fun/v2",
    beta: "https://api.beta.somon.fun/v2",
    alpha: "https://api.alpha.somon.fun/v2",
    dev: "https://api.dev.somon.fun/v2",
}[ENV] as string;
export const OSP_APP_ID = {
    prod: "1",
    pre: "50341201064231173",
    beta: "1",
    alpha: "1",
    dev: "1",
}[ENV] as string;
export const PFP_NFT_ADDRESS = {
    prod: "0x475Aa716337D79B5EA513BFE4cc01787eb2E6004",
    pre: "0x567206C8f1519094b60f117f9f2E51b8c04944ff",
    beta: "0x567206C8f1519094b60f117f9f2E51b8c04944ff",
    alpha: "0x567206C8f1519094b60f117f9f2E51b8c04944ff",
    dev: "0x567206C8f1519094b60f117f9f2E51b8c04944ff",
}[ENV] as string;
export const PRE_SALE_CONDITION_ADDRESS = {
    beta: "0x2210BA143E2c6144F11F23C4267E0830224F2dAF",
    alpha: "0xa9B4aFB4f53AD3012CbE5EF87EE46504Fe74541F",
    dev: "0xa9B4aFB4f53AD3012CbE5EF87EE46504Fe74541F",
}[ENV] as string;
const args = minimist(process.argv.slice(2),{string:['private-key']});
console.log(args['private-key'])
export const PRIVATE_KEY= args['private-key'] || process.env.PRIVATE_KEY as string;
