import * as fs from "node:fs";


export class LocalStorageProvider {
    dir = '.storage';

    private getKeyPath(key: string) {
        return `${this.dir}/${Buffer.from(key).toString('hex')}`;
    }

    constructor(dir?: string) {
        if (dir) {
            this.dir = dir;
        }
        if (!fs.existsSync(this.dir)) {
            fs.mkdirSync(this.dir, {recursive: true});
        }
    }

    getItem(key: string): string | null {
        const keyPath = this.getKeyPath(key);
        if (fs.existsSync(keyPath)) {
            const buffer = fs.readFileSync(keyPath);
            return buffer.toString('utf-8');
        }
        return null;
    }

    setItem(key: string, value: string): void {
        const keyPath = this.getKeyPath(key);
        fs.writeFileSync(keyPath, Buffer.from(value, 'utf-8'))
    }

    removeItem(key: string): void {
        const keyPath = this.getKeyPath(key);
        if (fs.existsSync(keyPath)) {
            fs.rmSync(keyPath);
        }
    }
}
