import {ethers} from "ethers";
import {ENV, OSP_APP_ID, PRIVATE_KEY} from "./config";
import {OspClient} from "@open-social-protocol/osp-client";
import {LocalStorageProvider} from "./storage";
import {GasSubsidyMiddleware, loginSomon} from "./somon";

const wallet = new ethers.Wallet(PRIVATE_KEY);
const storage = new LocalStorageProvider(`.storage/${wallet.address}`);

export async function getOspClient() {
    const ospClient = await OspClient.create({
        env: ENV,
        app_id: OSP_APP_ID,
        storage: storage
    });
    await ospClient.authentication.login(wallet);
    ospClient.txClient.addMiddleware(GasSubsidyMiddleware, 150);
    await loginSomon(ospClient, storage);
    return ospClient;
}

const oldFetch = fetch;
global.fetch = async function (
    input: string | URL | globalThis.Request,
    init?: RequestInit,
) {
    return oldFetch(input, init).then(async (res) => {
        const clone = res.clone();
        if (clone.status !== 200) {
            const data = await clone.json();
            console.log("fetch error", clone.status, clone.statusText, clone.url, data);
            const s = JSON.stringify(data);
            if (s.includes('0x37e8456b')) {
                throw new Error('must create profile first!');
            } else if (s.includes('0xecbc82c0')) {
                throw new Error('must join community!');
            }
        } else {
            const data = await clone.json();
            console.log("fetch res", clone.status, clone.statusText, clone.url, data);
        }
        return res;
    });
}

export async function catchError<T>(promise: Promise<T>): Promise<T> {
    try {
        return await promise;
    } catch (e: any) {
        const s = e.toString();
        if (s.includes('0xb9578cd2')) {
            throw new Error('join community failed: already joined!');
        } else if (s.includes('0xe14ddfc8')) {
            throw new Error('must create profile first!')
        }
        throw e;
    }
}
