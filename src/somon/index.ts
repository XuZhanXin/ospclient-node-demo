import {OspClient} from "@open-social-protocol/osp-client";
import {SOMON_URL} from "../config";
import {ISigner, Middleware, OperationOverrides, UserOperation} from "@open-social-protocol/osp-wallet";
import {ethers} from "ethers";
import {LocalStorageProvider} from "../storage";
import * as jose from "jose";

let jwtKey: string = '';

export async function loginSomon(ospClient: OspClient, storage: LocalStorageProvider) {
    const ospAddress = await ospClient.txClient.getAccountAddress();
    const key = `somon-${SOMON_URL}-${ospAddress}`;
    const jwt = storage.getItem(key);
    if (jwt) {
        const jwtPayload = jose.decodeJwt(jwt);
        if ((jwtPayload.exp as number - Math.ceil(Date.now() / 1000)) > 36000) {
            jwtKey = jwt;
            return;
        }
    }
    const ospIdToken = (await ospClient.authentication.idToken()).id_token;
    const response = await getResponse(
        await fetch(
            `${SOMON_URL}/customer/login/oauth?idToken=${ospIdToken}&publicKey=${ospAddress}&thirdAuthSource=OSP&autoRegister=true&platform=osp`,
            {
                headers: {
                    Saas_id: "monster",
                    "Content-Type": "application/json",
                },
                mode: "cors",
                method: "POST",
            }
        )
    );
    console.log("login somon res :", response);
    storage.setItem(key, response.jwtKey);
    jwtKey = response.jwtKey;
    return;
}

export async function getGasPolicy(userOperation: UserOperation) {
    const response = await getResponse(
        await fetch(`${SOMON_URL}/wallets/gas/policy`, {
            headers: {
                "Content-Type": "application/json",
                Jwt_token: jwtKey,
            },
            mode: "cors",
            method: "post",
            body: JSON.stringify({
                sender: userOperation.sender,
                nonce: ethers.utils.hexValue(userOperation.nonce),
                init_code: ethers.utils.hexlify(userOperation.initCode),
                call_data: ethers.utils.hexlify(userOperation.callData),
                call_gas_limit: ethers.utils.hexValue(userOperation.callGasLimit),
                verification_gas_limit: ethers.utils.hexValue(
                    userOperation.verificationGasLimit
                ),
                pre_verification_gas: ethers.utils.hexValue(
                    userOperation.preVerificationGas
                ),
                max_fee_per_gas: ethers.utils.hexValue(userOperation.maxFeePerGas),
                max_priority_fee_per_gas: ethers.utils.hexValue(
                    userOperation.maxPriorityFeePerGas
                ),
                paymaster_and_data: ethers.utils.hexlify(
                    userOperation.paymasterAndData
                ),
            }),
        })
    );
    console.log("get gas policy res :", response);
    return response;
}

// somon绑定eoa地址。
export async function bindWallet(ospClient:OspClient) {
    const customerInfo = await _getCustomerInfo();
    if(customerInfo.connectAddress){
        return customerInfo;
    }
    const signer = ospClient.txClient.signer;
    const address = await signer.getAddress();
    const challenge = await _getChallenge(address, ospClient.txClient.getEnvConfig().chain.network.chainId);
    return await _bind(address, await signer.signMessage(ethers.utils.toUtf8Bytes(challenge)));
}

async function _getChallenge(eoaAddress:string,chainId:number) {
    const response = await getKikiResponse(
        await fetch(`${SOMON_URL}/customer/auth/challenge?address=${eoaAddress}&chainId=${chainId}`, {
            headers: {
                "Content-Type": "application/json",
                Jwt_token: jwtKey,
            },
            mode: "cors",
            method: "post",
        })
    );
    return response.nonce;
}

async function _bind(eoaAddress:string,signature:string) {
    const response = await getResponse(
        await fetch(`${SOMON_URL}/customer/bind/auth?address=${eoaAddress}&signature=${signature}&thirdAuthSource=EXTERNAL_WALLET&platform=wallet_connect&walletType=evm`, {
            headers: {
                "Content-Type": "application/json",
                Jwt_token: jwtKey,
            },
            mode: "cors",
            method: "post",
        })
    );
    return response;
}

async function _getCustomerInfo() {
    const response = await getResponse(
        await fetch(`${SOMON_URL}/customer`, {
            headers: {
                "Content-Type": "application/json",
                Jwt_token: jwtKey,
            },
            mode: "cors",
            method: "get",
        })
    );
    return response;
}

export async function getTribePermit(tokenId:string) {
    const response = await getResponse(
        await fetch(`${SOMON_URL}/permit/${tokenId}`, {
            headers: {
                "Content-Type": "application/json",
                Jwt_token: jwtKey,
            },
            mode: "cors",
            method: "get",
        })
    );
    return response;
}

async function getKikiResponse(response: Response) {
    if (response.status != 200) {
        throw new Error(
            `Failed to fetch ${response.url}: ${response.status} ${response.statusText}`
        );
    }
    const json = await response.json();
    if (!json.success) {
        console.log(json);
        throw new Error(json);
    }
    return json;
}

async function getResponse(response: Response) {
    if (response.status != 200) {
        throw new Error(
            `Failed to fetch ${response.url}: ${response.status} ${response.statusText}`
        );
    }
    const json = await response.json();
    if (!json.success) {
        console.log(json);
        // throw new Error(json);
    }
    return json.obj;
}

export const GasSubsidyMiddleware: Middleware = async (
    userOperation: UserOperation,
    opt: OperationOverrides
): Promise<[UserOperation, OperationOverrides]> => {
    try {
        const gasPolicy = await getGasPolicy(userOperation);
        console.log(gasPolicy);
        opt.policy_data = gasPolicy.data;
    } catch (e) {
        console.log(e);
        throw e;
    }
    return [userOperation, opt];
};
